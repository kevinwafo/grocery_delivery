import React, { Fragment, useState, useEffect } from 'react'
import {BrowserRouter as Router, Route} from 'react-router-dom'

import { list } from '../data'

import {Navbar} from '../components'
import {CartPage} from './components/CartPage'
import {Home} from './components/Home'
import {Checkout} from './components/Checkout'
import {Confirm} from './components/Confirm'

import UserProfileContextProvider from '../lib/UserProfileContext'

import '../styles/App.css'


const App = props => {
  const {items, saveLocalStorage} = props
  const [category, setCategory] = useState(0)
  const [isFiltering, setFiltering] = useState(false)
  const [filtered, setFiltered] = useState(false)

  useEffect(()=>{
    saveLocalStorage(items)
  })

  const loadCategory = i => { setCategory(i) }

  const filterResult = value => {
    let fullList =  list.flat() //tous les produits non categorié
    let results = fullList.filter(item => {
      const name = item.name.toLowerCase()
      const term = value.toLowerCase()
      return name.indexOf(term) > -1
    })
    setFiltered(results)
  }

    return ( 
      <Fragment>
        <Router>
          <UserProfileContextProvider>
            <Navbar filter={filterResult}  setFiltering={setFiltering}/>
            {/* Routes */}
            <Route exact path='/' component={props => <Home 
                                                          loadCategory={loadCategory} 
                                                          category={category}
                                                          isFiltering={isFiltering} 
                                                          filtered={filtered}
                                                          list={list}/>} />  
            <Route path='/cart' component={CartPage} />
            <Route path='/checkout' component={Checkout} />
            <Route path='/delivery' component={Confirm} />
          </UserProfileContextProvider>
        </Router>
      </Fragment>
      
    )

}

export default App