import React, { Fragment, useState, useEffect, useContext } from 'react';
import { Link } from "react-router-dom";
import "../../styles/App.css";

import {UserProfileContext} from '../../lib/UserProfileContext'

export const Checkout = () => {

    const value = useContext(UserProfileContext)
    const {
        firstName,
        lastName,
        email,
        zipCode,
        address,
        city,
        setUserProfileContext
    } = value

    const [isValid, setIsValid] = useState(false)
    const validation = () => {
        let errors = []
        const inputs = document.querySelectorAll('.validation-cont')
        inputs.forEach(input => {
            if(!input.value){
                errors.push(input)
            }
        })
        setIsValid(!errors.length) 
        console.log(errors)
    }
    
    useEffect(() => {
        validation()
    })

    return (
      <Fragment>
        <div className='container'>
            <div className="col-sm-6 offset-3">
            <h2>Checkout</h2>
            <br />
            <form>
                <div className="row">
                <div className="col">
                    <input
                        type="text"
                        className="form-control validation-cont"
                        placeholder="First name"
                        property=""
                        name='firstName'
                        defaultValue={firstName}
                        onChange={(e) => { setUserProfileContext({[e.target.name]: e.target.value}) } }
                    />
                </div>
                <div className="col">
                    <input
                        type="text"
                        className="form-control validation-cont"
                        placeholder="Last name"
                        property=""
                        name='lastName'
                        defaultValue={lastName}
                        onChange={(e) => { setUserProfileContext({[e.target.name]: e.target.value}) } }
                    />
                </div>
                </div>
                <br />
                <div className="form-group">
                    <input
                        type="email"
                        className="form-control validation-cont"
                        id="exampleInputEmail1"
                        placeholder="Email address"
                        property=""
                        name="email"
                        defaultValue={email}
                        onChange={(e) => { setUserProfileContext({[e.target.name]: e.target.value}) } }
                    />
                    <small id="emailHelp" className="form-text text-muted">
                        We'll never share your email with anyone else.
                    </small>
                </div>
                <div className="form-group">
                    <input
                        type="text"
                        className="form-control validation-cont"
                        id="exampleInputEmail1"
                        placeholder="Address"
                        property=""
                        name='address'
                        defaultValue={address}
                        onChange={(e) => { setUserProfileContext({[e.target.name]: e.target.value}) } }
                    />
                </div>
                <div className="row">
                <div className="col">
                    <input
                        type="text"
                        className="form-control validation-cont"
                        placeholder="Postal Code"
                        property=""
                        defaultValue={zipCode}
                        name='zipCode'
                        onChange={(e) => { setUserProfileContext({[e.target.name]: e.target.value}) } }
                    />
                </div>
                <div className="col">
                    <input
                        type="text"
                        className="form-control validation-cont"
                        placeholder="City"
                        property=""
                        name='city'
                        defaultValue={city} 
                        onChange={(e) => { setUserProfileContext({[e.target.name]: e.target.value}) } }
                    />
                </div>
                </div>
                <br />
                <Link to='/delivery' className={`btn btn-light btn-lg btn-block checkout ${!isValid && 'disabled' } bg-crimson`}>
                    Confirm
                </Link>
            </form>
            </div>
        </div>
      </Fragment>
    );
}