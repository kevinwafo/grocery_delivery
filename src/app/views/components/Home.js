import React from 'react'

import {List} from '../../components'



const SideMenu = ({loadCategory, category}) => {
    const links = ['Fruits', 'Legumes', 'Produits frais', 'Epiceries', 'Boissons']
    return ( 
      <div className='col-sm-2 sidebar'>
        <ul>
          {links.map((link, index) =>
             <li 
              className={category === index ? 'active' : null}
              key={link} 
              onClick={ () => {
                loadCategory(index)
                
              } }
              > 
              {link} 
            </li> 
            )}
        </ul>
      </div>
    )
}

export const Home = ({
    loadCategory,
    category,
    isFiltering,
    filtered,
    list  }) => {

    return (
        <div className='container'>
            <div className='row'>
                <SideMenu loadCategory={loadCategory} category={category}/>
                <List data={isFiltering ? filtered : list[category]} />
            </div>
        </div>
    )
    
}
